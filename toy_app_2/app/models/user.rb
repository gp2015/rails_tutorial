class User < ApplicationRecord
  has_many :microposts
  validates :name, presence: true
  validates :email, presence: true
  
  def get_first_post
    
    @user = self
    @first_post = @user.microposts.first
    
    if  @first_post == nil
      @val = "No posts"
      return @val
    else
      @val = @first_post[:content]
      return @val
    end
  end
end
