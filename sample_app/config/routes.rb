Rails.application.routes.draw do
  root 'static_pages#home' # or just "root 'static_pages#home'"
  get '/help', to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact' # routes /contact URL to the contact method in static_pages_controller
  # get 'static_pages/contact' # just a URL to contact method in static_pages_controller
  get '/signup', to: 'users#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
